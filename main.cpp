#include "main.h"
#include "version.h"

#ifndef SERVER
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQmlComponent>
#include <QProcess>
#include <QDir>
#include <QDirIterator>
#include <QtAndroidExtras/QtAndroid>
#include <QtQuickControls2/QQuickStyle>
#else
#include "server.h"

#include <getopt.h>
#endif

QSettings *set = NULL, *mainset = NULL;
QString homedir;

static bool restart = false;

#ifndef SERVER
QSoundButtons::QSoundButtons(QQmlApplicationEngine *engine_pointer, QGuiApplication *app_pointer)
{
	engine = engine_pointer;
	app = app_pointer;
}
#else
QSoundButtons::QSoundButtons(QCoreApplication *app_pointer)
{
	app = app_pointer;
}
#endif

const QString QSoundButtons::GetPositionString(const int button)
{
	int y = (button-1) / columns + 1;
	int x = button - (y-1)*columns;

	set->beginGroup("Global");
	bool landscape = set->value("landscape", false).toBool();
	set->endGroup();

	if (landscape)
		return QString::number(y) + "x" + QString::number(x);

	return QString::number(x) + "x" + QString::number(y);
}

int QSoundButtons::GetPositionNumber(const QString button)
{
	QStringList coords = button.split("x");
	int x = coords[0].toInt();
	int y = coords[1].toInt();

	set->beginGroup("Global");
	bool landscape = set->value("landscape", false).toBool();
	set->endGroup();

	if (landscape)
		return ((x-1)*rows)+y;

	return ((y-1)*rows)+x;
}

#ifndef SERVER
void QSoundButtons::SetButtonAnimation(int id, bool active) // setRunningAnim(id, run)
{
	QObject *root = engine->rootObjects()[0];
	QObject *object = root->findChild<QObject*>("buttons");
	QMetaObject::invokeMethod(object, "setRunningAnim", Q_ARG(QVariant, QVariant(id)), Q_ARG(QVariant, QVariant(active)));
}

unsigned int QSoundButtons::getButtonSoundLength(const int button)
{
#ifndef NO_FMOD
	if (client.active)
		return 500; // fake
	return FMOD_GetSoundLength(button);
#else
	return 0;
#endif
}

unsigned int QSoundButtons::getButtonChannelPosition(const int button)
{
#ifndef NO_FMOD
	return FMOD_GetPosition(button);
#else
	return 0;
#endif
}
#endif

#ifndef NO_FMOD
void QSoundButtons::PreCreateSounds()
{
	// Get system configuration
	mainset->beginGroup("System");
	bool hardware = mainset->value("hardware", false).toBool();
	mainset->endGroup();

	FMOD_MODE mode= FMOD_2D;
	if (hardware)
		mode |= FMOD_HARDWARE;
	else
		mode |= FMOD_SOFTWARE;

	// Pre-process all available sounds
	for (int i = 1; i <= rows*columns; i++)
	{
		set->beginGroup("button" + GetPositionString(i));
		QString action = set->value("action").toString();

		// ignore other commands
		if (action != "play")
		{
			set->endGroup();
			continue;
		}

		QString data = set->value("data").toString();

		// ignore empty variables
		if (data.isEmpty() || data.isNull())
		{
			set->endGroup();
			continue;
		}

		QString loop = set->value("loop", "off").toString();

		FMOD_MODE loopmode = FMOD_LOOP_OFF;
		if (loop == "off")
			loopmode = FMOD_LOOP_OFF;
		else if (loop == "on")
			loopmode = FMOD_LOOP_NORMAL;
		else if (loop == "bidi")
			loopmode = FMOD_LOOP_BIDI;

		QString note = set->value("note", "C-5").toString();

		set->endGroup();

		// Process keywords
		ProcessForKeyWords(data);
		// replace ! with program directory
		mainset->beginGroup("Preset");
		QString preset = mainset->value("preset").toString();
		mainset->endGroup();
		data.replace("!", HomeDirectory() + preset + QDir::separator());

		FMOD_PreCreateSound(i, data, mode | loopmode, note);

		Log("Pre-loading sound: button=" + GetPositionString(i) + " data=" + data + " note=" + note);
	}
}
#endif

void QSoundButtons::play(const int button)
{
	// get developer mode
	mainset->beginGroup("System");
	bool developer = mainset->value("developer", false).toBool();
	mainset->endGroup();

	// use midi or not
	set->beginGroup("Global");
#ifdef SERVER
	bool usemidi = set->value("usemidi", false).toBool();
#endif
	set->endGroup();

#ifdef SERVER
	if (usemidi)
	{
		QRTMIDI_MSG message;

		// Get bindings from the config
		set->beginGroup("button" + GetPositionString(button));
		QString midimsgs = set->value("midiplay").toString();
		QStringList msglist = midimsgs.split("|");
		set->endGroup();

		for (int j = 0; j < msglist.size(); j++)
		{
			if (msglist[j].isEmpty())
				break;
			QStringList cmdlist = msglist[j].split(",");
			for (int i = 0; i < cmdlist.size(); i++)
			{
				// parse for hex
				if (cmdlist[i].startsWith("0x"))
					message.push_back(cmdlist[i].toUInt(0, 16));
				else
					message.push_back(cmdlist[i].toUInt());

				if (developer)
				{
					if (i==0)
						Log("buttons.midi.play: midiplay=" + cmdlist[i], false);
					else if (i==cmdlist.size()-1)
						Print("," + cmdlist[i] + "\n");
					else
						Print("," + cmdlist[i]);
				}
			}

			RTMIDI_SendMessage(message);
			message.clear();
		}
		return;
	}
#endif

#ifndef SERVER
	if (client.active)
	{
		client.SendPacket(PT_PLAY, button);
		return;
	}
#endif

#ifndef NO_FMOD
	// Get bindings from the config
	set->beginGroup("button" + GetPositionString(button));

	// basic
	QString action = set->value("action").toString();
	QString data = set->value("data").toString();

	// others
	QString channelmode = set->value("channelmode", "free").toString();

	float volume = set->value("volume", 1.0).toFloat();
	float panning = set->value("panning", 0).toFloat();

	bool backwards = set->value("backwards", false).toBool();
	int stoppolicy = set->value("stoppolicy", 0).toInt();

	set->endGroup();

	if (action == "play")
	{
		if (stoppolicy == 2)
		{
			if (used[button])
			{
				FMOD_Stop(button);
				used[button] = false;
				return;
			}
			else
				used[button] = true;
		}

		FMOD_CHANNELINDEX channelindex = FMOD_CHANNEL_FREE;
		if (channelmode == "reuse")
			channelindex = FMOD_CHANNEL_REUSE;
		else if (channelmode == "free")
			channelindex = FMOD_CHANNEL_FREE;

		FMOD_Play(button, backwards, channelindex);
		FMOD_SetVolume(button, volume);
		FMOD_SetPanning(button, panning);

		if (developer)
			Log("sbuttons.play: action=" + action + " button=" + GetPositionString(button) + " data=" + data + " volume=" + QString::number(volume) + " panning=" + QString::number(panning) + " backwards=" + BoolQStr(backwards) + " channelmode=" + channelmode);
	}
	else if (action == "stop")
	{
		QStringList stoplist = data.split(',');
		for (int i = 0; i < stoplist.size(); i++)
		{
			int btn = GetPositionNumber(stoplist[i]);
			FMOD_Stop(btn);
			used[btn] = false;
#ifndef SERVER
			SetButtonAnimation(i, false);
#endif
		}

		if (developer)
			Log("sbuttons.play: action=" + action + " button=" + GetPositionString(button) + " data=" + data);
	}
	else if (action == "stopall")
	{
		for (int i = 1; i <= rows*columns; i++)
		{
			FMOD_Stop(i);
			used[i] = false;
#ifndef SERVER
			SetButtonAnimation(i, false);
#endif
		}

		if (developer)
			Log("sbuttons.play: action=" + action + " button=" + GetPositionString(button) + " data=" + data);
	}
#endif
}

void QSoundButtons::stop(const int button)
{
	// get developer mode
	mainset->beginGroup("System");
	bool developer = mainset->value("developer", false).toBool();
	mainset->endGroup();

	// use midi or not
	set->beginGroup("Global");
#ifdef SERVER
	bool usemidi = set->value("usemidi", false).toBool();
#endif
	set->endGroup();

#ifdef SERVER
	if (usemidi)
	{
		QRTMIDI_MSG message;

		// Get bindings from the config
		set->beginGroup("button" + GetPositionString(button));
		QString midimsgs = set->value("midistop").toString();
		QStringList msglist = midimsgs.split("|");
		set->endGroup();

		for (int j = 0; j < msglist.size(); j++)
		{
			if (msglist[j].isEmpty())
				break;
			QStringList cmdlist = msglist[j].split(",");
			for (int i = 0; i < cmdlist.size(); i++)
			{
				// parse for hex
				if (cmdlist[i].startsWith("0x"))
					message.push_back(cmdlist[i].toUInt(0, 16));
				else
					message.push_back(cmdlist[i].toUInt());

				if (developer)
				{
					if (i==0)
						Log("buttons.midi.stop: midistop=" + cmdlist[i], false);
					else if (i==cmdlist.size()-1)
						Print("," + cmdlist[i] + "\n");
					else
						Print("," + cmdlist[i]);
				}
			}

			RTMIDI_SendMessage(message);
			message.clear();
		}
		return;
	}
#endif

#ifndef SERVER
	if (client.active)
	{
		client.SendPacket(PT_STOP, button);
		return;
	}
#endif

#ifndef NO_FMOD
	// Get bindings from the config
	set->beginGroup("button" + GetPositionString(button));
	int stoppolicy = set->value("stoppolicy", 0).toInt();
	QString loop = set->value("loop", "off").toString();
	QString data = set->value("data").toString();
	set->endGroup();

	if (data == "stop" || data == "stopall")
		return;

	if (stoppolicy == 1 || (stoppolicy == 0 && loop != "off"))
		FMOD_Stop(button);

	if (developer)
		Log("sbuttons.stop: button=" +  GetPositionString(button) + " stoppolicy=" + QString::number(stoppolicy) + " loop=" + loop );
#endif
}

void QSoundButtons::restartApplication()
{
	mainset->sync();

#ifdef SERVER
	mainset->beginGroup("Server");
	bool sync = mainset->value("presetsync", true).toBool();
	if (!sync)
		return;
	mainset->endGroup();
#endif

	restart = true;
	app->instance()->quit();
}

void QSoundButtons::applyPreset(QString preset)
{
	mainset->beginGroup("Preset");
	mainset->setValue("preset", QVariant(preset));
	mainset->endGroup();

#ifndef SERVER
	if (client.active)
		client.SendPacketString(PT_PRESET, preset);
#else
	mainset->beginGroup("Server");
	bool sync = mainset->value("presetsync", true).toBool();
	mainset->endGroup();
	if (!sync)
		return;
#endif

	restartApplication();
}

#ifndef SERVER

void QSoundButtons::applySettings(QString address, QString port, bool connect)
{
	mainset->beginGroup("Client");
	mainset->setValue("address", address);
	mainset->setValue("port", port.toUShort());
	mainset->setValue("connect", connect);
	mainset->endGroup();

	restartApplication();
}

void QSoundButtons::downloadDefaultPreset()
{
	download_manager = new QFileDownloader(QUrl(QString(DEFAULT_PRESET_URL)), this);
	connect(download_manager, SIGNAL(downloaded()), this, SLOT(preset_downloaded()));
	connect(download_manager, SIGNAL(statusUpdate()), this, SLOT(download_status_changed()));
}

void QSoundButtons::preset_downloaded()
{
	QByteArray archive = download_manager->downloadedData();
	Log("File downloaded into memory: " + QString(DEFAULT_PRESET_URL) + " (total: " + QString::number(archive.size()) + " bytes)");
	delete download_manager;

	QString path = QString(PROGDIR) + "launchpad-preset.tmp.zip";
	QFile file(path);
	if (!file.open(QFile::WriteOnly))
	{
		Log("Error opening file for writing: " + path);
		return;
	}

	file.write(archive);
	file.close();

	QString command = "/system/bin/tar -xf " + path + " -C " + QString(PROGDIR);
	Log("Running command: " + command);
	QProcess unzip;
	unzip.start(command);
	unzip.waitForFinished();

	QFile clean(path);
	clean.remove();

	QFile test(QString(PROGDIR) + QString(INIFILE));
	QQmlContext *root = engine->rootContext();
	if (test.exists())
	{
		Log("Preset installed successfully!");
		root->setContextProperty("presetInstallSuccess", true);
	}
	else 
	{
		Log("Preset install error happened. Check " + QString(PROGDIR));
		root->setContextProperty("presetInstallError", true);
	}
}

void QSoundButtons::download_status_changed()
{
	Log("Downloading progress: " + QString::number(download_manager->getStatusValue()*100) + "%");
	QQmlContext *root = engine->rootContext();
	root->setContextProperty("downloadProgress", download_manager->getStatusValue());
}

QVariant QSoundButtons::generatePresetList()
{
	mainset->beginGroup("Preset");
	QString preset = mainset->value("preset").toString();
	mainset->endGroup();

	QStringList presets;
	QDirIterator it(QString(PROGDIR), QDirIterator::Subdirectories);
	while (it.hasNext()) 
	{
		QString fn = it.next();
		QFileInfo info(fn);
		if (info.fileName() == QString(PRESETINI))
		{
			QStringList path = fn.split(QDir::separator());
			presets.push_back(path[path.size()-2]);
		}
	}
	// put selected preset on top
	if (!presets.isEmpty())
		presets.move(presets.indexOf(preset), 0);

	return QVariant(presets);
}

const QString QSoundButtons::getPressedButtonColor(const int button)
{
	// Get bindings from the config
	set->beginGroup("Global");
	QString gcolor = set->value("pressedbuttoncolor", "red").toString();
	set->endGroup();

	set->beginGroup("button" + GetPositionString(button));
	QString color = set->value("pressedcolor", gcolor).toString();
	set->endGroup();

	return color;
}

const QString QSoundButtons::getButtonColor(const int button)
{
	// Get bindings from the config
	set->beginGroup("Global");
	QString gcolor = set->value("buttoncolor", "white").toString();
	set->endGroup();

	set->beginGroup("button" + GetPositionString(button));
	QString color = set->value("color", gcolor).toString();
	set->endGroup();

	return color;
}

const QString QSoundButtons::getButtonBorderColor(const int button)
{
	// Get bindings from the config
	set->beginGroup("Global");
	QString gcolor = set->value("buttonbordercolor", "black").toString();
	set->endGroup();

	set->beginGroup("button" + GetPositionString(button));
	QString color = set->value("bordercolor", gcolor).toString();
	set->endGroup();

	return color;
}

bool QSoundButtons::getButtonColorPolicy(const int button)
{
	// Get bindings from the config
	set->beginGroup("button" + GetPositionString(button));
	int stoppolicy = set->value("stoppolicy", 0).toInt();
	set->endGroup();

	if (stoppolicy < 2)
		return false;

	return true;
}
#endif

#ifdef SERVER
void PrintInfo()
{
	printf("LaunchpadD is a fully configurable music creation tool for android platform.\n");
	printf("Developed by Monsterovich. :)\n");
	printf("\n");
#ifndef Q_OS_WIN32
	printf("Usage: launchpadd-server <homedir> (options)\n");
#else
	printf("Usage: launchpadd-server.exe <homedir> (options)\n");
#endif
	printf("\n");
	printf("Options:\n");
	printf("-h    Print this message.\n");
	printf("-v    Print program version and quit.\n");
	printf("-m    Print available MIDI ports (info) and quit.\n");
	printf("\n");
	printf("You can find more information about launchpadd configuration here:\n");
	printf("https://bitbucket.org/Monsterovich/launchpadd/wiki/\n");
}
#endif

int main(int argc, char *argv[])
{
	int status = -1;
	QQuickStyle::setStyle("Material");
	while (true) {
#ifdef SERVER
	char opt;
	while ((opt = getopt(argc, argv, ":hvm")) != -1) {
		switch (opt) {
		case 'v':
			printf("%s\n", VERSION_STRING);
			return 1;
		case 'm':
			Log("RTMidi version: " + QString(RTMIDI_VERSION) + " (bundled with '" + RTMIDI_GetApiName() + "')");
			RTMIDI_Init();
			Log(SEPARATOR);
			RTMIDI_PrintPortInfo();
			RTMIDI_DeInit();
			return 1;
		case 'h':
			PrintInfo();
			return 1;
		default:
			printf("Unknown option. Use '-h' option for more info.\n");
			return 1;
		}
	}

	homedir = QString(argv[1]);

	if (homedir.isEmpty())
	{
		printf("You must specify a home directory! Use '-h' option for more info.\n");
		return 1;
	}

	// add dir separator if missing
	if (homedir[homedir.size()-1] != QDir::separator())
		homedir += QDir::separator();

#endif

	CreateHomeDirectory();
	CreateNoMediaFile();
	CreateLogFile();

#ifndef SERVER
	Log("Launchpadd is running!");
#else
	Log("Launchpadd server is running!");
#endif
	Log("Version: " + QString(VERSION_STRING));
	Log("OS version: " + QSysInfo::prettyProductName());

#ifndef SERVER
	QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
	QGuiApplication app(argc, argv);

	// QML Engine
	QQmlApplicationEngine engine;

#else
	QCoreApplication app(argc, argv);
#endif

	// Init sound buttons
#ifndef SERVER
	QSoundButtons sbuttons(&engine, &app);
#else
	QSoundButtons sbuttons(&app);
#endif
#ifndef SERVER
	QQmlContext *root = engine.rootContext();
	root->setContextProperty("sbuttons", &sbuttons);

	// set fake values to remove warnings (these variables will be used later)
	root->setContextProperty("downloadProgress", 0.0f);
	root->setContextProperty("presetInstallSuccess", 0);
	root->setContextProperty("presetInstallError", 0);

	// screenPixelDensity value
	const qreal screenPixelDensity = round((app.primaryScreen()->physicalDotsPerInch()*app.primaryScreen()->devicePixelRatio()) / 160.0);
	root->setContextProperty("screenPixelDensity", screenPixelDensity);
#endif

	// iniExists value
	const QString filename = HomeDirectory() + INIFILE;
	QFile checker(filename);
#ifndef SERVER
	root->setContextProperty("iniExists", checker.exists());
#else
	if (!checker.exists())
	{
		Log("Error: Main ini file 'launchpadd.ini' is not found in '" + HomeDirectory() + "'!");
		exit(1);
	}
#endif


	// Init main settings file
	mainset = new QSettings(filename, QSettings::IniFormat);

#ifndef SERVER
	// iniNull value
	root->setContextProperty("iniNull", mainset == NULL);
#endif

	// Load preset ini
	mainset->beginGroup("Preset");
	QString preset = mainset->value("preset").toString();
	mainset->endGroup();

	// presetiniExists value
	const QString presetfilename = HomeDirectory() + preset + QDir::separator() + PRESETINI;
	QFile checker2(presetfilename);
#ifndef SERVER
	root->setContextProperty("presetiniExists", checker2.exists());
#else
	if (!checker2.exists())
	{
		Log("Error: Preset ini file is not found '" + HomeDirectory() + preset + QDir::separator() + "'!");
		return 1;
	}
#endif

	// Init preset file
	set = new QSettings(presetfilename, QSettings::IniFormat);
	Log("Loading preset '" + preset + "'");

#ifndef SERVER
	// presetiniNull value
	root->setContextProperty("presetiniNull", set == NULL);

#endif

	// Global values
	set->beginGroup("Global");
	int columns = set->value("columns", 5).toInt();
	int rows = set->value("rows", 7).toInt();

#ifdef SERVER
#ifndef NO_FMOD
	bool usemidi = set->value("usemidi", false).toBool();
	Log("usemidi=" + BoolQStr(usemidi));
#else
	bool usemidi = true;
#endif

#else
	bool usemidi = false;
#endif

#ifndef SERVER
	QString backgroundcolor = set->value("backgroundcolor", "gray").toString();
#endif
	set->endGroup();

	// for the actions
	sbuttons.SetSize(rows, columns);

#ifndef SERVER
	root->setContextProperty("padColumns", columns);
	root->setContextProperty("padRows", rows);
	root->setContextProperty("backgroundColor", backgroundcolor);
	root->setContextProperty("restart", restart);

	if (restart)
		restart = false;
#endif

	// Log stuff
#ifndef SERVER
	Log("!screenPixelDensity=" + QString::number(screenPixelDensity));
#endif
	Log("!homeDirectory=" + HomeDirectory());
	Log(SEPARATOR);
	Log("columns=" + QString::number(columns));
	Log("rows=" + QString::number(rows));

#ifndef SERVER
	mainset->beginGroup("Client");
	bool connect = mainset->value("connect", false).toBool();
	QString address = mainset->value("address", "").toString();
	quint16 port = mainset->value("port", 3455).toInt();

	// client settings
	root->setContextProperty("server_address", address);
	root->setContextProperty("server_port", port);
	root->setContextProperty("server_connect", connect);

	mainset->endGroup();
#else
	bool connect = false;
#endif

#ifndef SERVER
	Log("connect=" + BoolQStr(connect));
	Log("address=" + address);
	Log("port=" + QString::number(port));
#endif

// Deprecated
#ifdef SERVER
	mainset->beginGroup("System");
	if (mainset->childKeys().contains("usemidi"))
		Log("WARNING: the use of usemidi in System block is deprecated and won't work! This value should be moved to Global block of your preset.");
	mainset->endGroup();
#endif

#ifndef NO_FMOD
	if (!connect && !usemidi)
	{
		mainset->beginGroup("System");

		QString output = mainset->value("output", "auto").toString();
		unsigned bufferlength = mainset->value("bufferlength", 16).toUInt();
		int numbuffers = mainset->value("numbuffers", 128).toInt();

		mainset->endGroup();

		// more fmod settings
		FMOD_OUTPUTTYPE outputtype = FMOD_OUTPUTTYPE_AUTODETECT;
		if (output == "auto")
			outputtype = FMOD_OUTPUTTYPE_AUTODETECT;
		// android
		else if (output == "audiotrack")
			outputtype = FMOD_OUTPUTTYPE_AUDIOTRACK;
		else if (output == "opensl")
			outputtype = FMOD_OUTPUTTYPE_OPENSL;
		// linux
		else if (output == "alsa")
			outputtype = FMOD_OUTPUTTYPE_ALSA;
		else if (output == "oss")
			outputtype = FMOD_OUTPUTTYPE_OSS;
		else if (output == "pulseaudio")
			outputtype = FMOD_OUTPUTTYPE_PULSEAUDIO;
		else if (output == "esd")
			outputtype = FMOD_OUTPUTTYPE_ESD;
		// more platforms for server
#ifdef SERVER
		// windows
		else if (output == "dsound")
			outputtype = FMOD_OUTPUTTYPE_DSOUND;
		else if (output == "winmm")
			outputtype = FMOD_OUTPUTTYPE_WINMM;
		else if (output == "wasapi")
			outputtype = FMOD_OUTPUTTYPE_WASAPI;
		else if (output == "asio")
			outputtype = FMOD_OUTPUTTYPE_ASIO;
		// mac
		else if (output == "coreaudio")
			outputtype = FMOD_OUTPUTTYPE_COREAUDIO;
#endif

		FMOD_Init(outputtype, bufferlength, numbuffers);
		FMOD_CheckVersion();

		Log("output=" + output);
		Log("bufferlength=" + QString::number(bufferlength));
		Log("numbuffers=" + QString::number(numbuffers));
	}
#endif

#ifdef SERVER
	if (usemidi)
	{
		Log("RTMidi version: " + QString(RTMIDI_VERSION) + " (bundled with '" + RTMIDI_GetApiName() + "')");
		RTMIDI_Init();
		Log(SEPARATOR);
		RTMIDI_PrintPortInfo();
		Log(SEPARATOR);

		mainset->beginGroup("System");
		unsigned midiport = mainset->value("midiport", 0).toUInt();
		mainset->endGroup();

		Log("Openning MIDI port: midiport=" + QString::number(midiport));

		RTMIDI_OpenPort(midiport);
	}
#endif

#ifndef NO_FMOD
	// pre process sounds for the best performance
	if (!usemidi && !connect)
		sbuttons.PreCreateSounds();
#endif

#ifndef SERVER
	if (connect)
	{
		sbuttons.client.active = true;
		sbuttons.client.Connect(address, port);
		sbuttons.client.SendPacketString(PT_PRESET, preset);
	}
	root->setContextProperty("clientsideonly", connect);
#endif

#ifndef SERVER
	Log("Starting UI...");
	engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
	status = app.exec();
#else

	mainset->beginGroup("Server");
	quint16 port = mainset->value("port", 3455).toInt();
	mainset->endGroup();

	UDPServer net(port, &sbuttons);
	(void)net;

	status = app.exec();
#endif
	CloseLog();

	if (!connect)
	{
#ifdef SERVER
		if (usemidi)
			RTMIDI_DeInit();
		else
#endif
#ifndef NO_FMOD
			FMOD_DeInit();
#else
		{}
#endif
	}

#ifndef SERVER
	if (connect)
		sbuttons.client.SendPacket(PT_BYE);
#endif

	if (restart)
		continue;
	else
		break;

	}
	return status;
}

