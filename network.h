#ifndef NETWORK_H
#define NETWORK_H

enum PacketType {
	PT_PLAY = 1,
	PT_STOP,
	PT_HELLO,
	PT_BYE,
	PT_PRESET
};

#endif // NETWORK_H

