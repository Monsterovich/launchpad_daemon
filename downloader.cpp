#include "downloader.h"

QFileDownloader::QFileDownloader(QUrl Url, QObject *parent) : QObject(parent)
{
	connect(&m_WebCtrl, SIGNAL (finished(QNetworkReply*)),this, SLOT (fileDownloaded(QNetworkReply*)));
	QNetworkRequest request(Url);
	QNetworkReply *reply = m_WebCtrl.get(request);
	connect(reply, SIGNAL(downloadProgress(qint64, qint64)), this, SLOT(progressChanged(qint64, qint64)));
}

void QFileDownloader::fileDownloaded(QNetworkReply* pReply) 
{
	m_DownloadedData = pReply->readAll();
	//emit a signal
	pReply->deleteLater();
	emit downloaded();
}

void QFileDownloader::progressChanged(qint64 read, qint64 total)
{
	m_Read = read;
	m_Total = total;
	emit statusUpdate();
}

double QFileDownloader::getStatusValue()
{
	return (double)m_Read / m_Total;
}

const QByteArray QFileDownloader::downloadedData()
{
	return m_DownloadedData;
}
