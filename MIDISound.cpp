#include "MIDISound.h"

RtMidiOut *midiout = NULL;

void RTMIDI_Init()
{
	try 
	{
		midiout = new RtMidiOut();
	}
	catch (RtMidiError &error) 
	{
		QString errmsg = error.getMessage().c_str();
		Log("Rtmidi error: " + errmsg);
		Crash();
	}
}

void RTMIDI_DeInit()
{
	delete midiout;
}

void RTMIDI_OpenPort(unsigned port)
{
	try
	{
		midiout->openPort(port);
	}
	catch (RtMidiError &error)
	{
		QString errmsg = error.getMessage().c_str();
		Log("Rtmidi error: " + errmsg);
		Crash();
	}
}

void RTMIDI_SendMessage(QRTMIDI_MSG message)
{
	std::vector <uchar> msg = message.toStdVector();
	midiout->sendMessage(&msg);
}

void RTMIDI_PrintPortInfo()
{
	QString pn;
	unsigned nPorts = midiout->getPortCount();

	if (nPorts > 1)
		Log("There are " + QString::number(nPorts) + " MIDI output ports available.");
	else if (nPorts == 1)
		Log("There is " + QString::number(nPorts) + " MIDI output port available.");
	else
	{
		Log("There are no MIDI output ports available! Saaad D:");
		return;
	}

	for (unsigned i = 0; i < nPorts; i++)
	{
		try 
		{
			pn = QString(midiout->getPortName(i).c_str());
		}
		catch (RtMidiError &error) 
		{
				QString errmsg = error.getMessage().c_str();
				Log("Rtmidi error: " + errmsg);
				Crash();
		}
		Log("Output Port #" + QString::number(i) + ": " + pn);
	}
}

const QString RTMIDI_GetApiName()
{
#if defined(__UNIX_JACK__)
	return "jack";
#endif
#if defined(__LINUX_ALSA__)
	return "alsa";
#endif
#if defined(__WINDOWS_MM__)
	return "winmm";
#endif
#if defined(__MACOSX_CORE__)
	return "coreaudio";
#endif
#if defined(__RTMIDI_DUMMY__)
	return "unknown";
#endif
}
