#ifndef CLIENT_H
#define CLIENT_H

#include "misc.h"
#include "network.h"

class UDPClient : public QObject
{
	Q_OBJECT
public:
	explicit UDPClient(QObject *parent = 0);
	void SendPacket(PacketType type, quint16 data = 0);
	void SendPacketString(PacketType type, QString str);
	void Connect(QString address_str, quint16 newport);

	bool active;

private:
	void appendInt16(QByteArray &array, quint16 value);
	void appendInt8(QByteArray &array, quint8 value);

private:
	QHostAddress address;
	quint16 port;
	QUdpSocket *socket;
};

#endif // CLIENT_H
