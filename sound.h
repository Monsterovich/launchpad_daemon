#ifndef SOUND_H
#define SOUND_H

#include "misc.h"

void GetFrequencyRatio(float &freq, const QString note);

#endif // SOUND_H
