TEMPLATE = app

# BASIC
QT += network
CONFIG += android

# fmod library on/off
!no_fmod {
    !android {
	contains(QT_ARCH, i386) {
	    LIBS += -lfmodex
	}else {
	    LIBS += -lfmodex64
	}
    }
    android {
	LIBS += -lfmodex
    }
}

# if not android, enable server mode
!android {
CONFIG += server
}

# enable console support in windows
windows {
CONFIG += console
}

# add icon in windows
windows {
RC_FILE = launchpadd-server.rc
}

# libs for the client
!server {
QT += qml quick androidextras quickcontrols2
}

# remove gui library from the server
server {
QT -= gui
}

# change name of server app
server {
        linux {
            TARGET = launchpadd-server-bin
        }
        windows {
            TARGET = launchpadd-server
        }
}

# common sources
SOURCES += main.cpp FMODSound.cpp \
    misc.cpp \
    sound.cpp \

# client/server sources
server {
    SOURCES += server.cpp rtmidi/RtMidi.cpp MIDISound.cpp

    # ignore fmod stuff
    no_fmod {
    SOURCES -= FMODSound.cpp sound.cpp
    }
}
!server {
    SOURCES += client.cpp downloader.cpp
}

# common headers
HEADERS += \
    main.h \
    FMODSound.h \
    common.h \
    misc.h \
    sound.h \
    version.h \
    network.h

# client/server headers
server {
    HEADERS += server.h MIDISound.h rtmidi/RtMidi.h
}

!server {
    HEADERS += client.h downloader.h
}

# client android files & qrc
!server {
    RESOURCES += qml.qrc

    # Additional import path used to resolve QML modules in Qt Creator's code model
    QML_IMPORT_PATH =

    # Default rules for deployment.
    include(deployment.pri)

    DISTFILES += \
        android/AndroidManifest.xml \
        android/gradle/wrapper/gradle-wrapper.jar \
        android/gradlew \
        android/res/values/libs.xml \
        android/build.gradle \
        android/gradle/wrapper/gradle-wrapper.properties \
        android/gradlew.bat
}

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

    android {
        equals(ANDROID_TARGET_ARCH, armeabi-v7a) { 
            LIBS += -L$$PWD/libs/armv7/
            ANDROID_EXTRA_LIBS = $$PWD/libs/armv7/libfmodex.so $$PWD/libs-openssl/arch-armeabi-v7a/libssl.so $$PWD/libs-openssl/arch-armeabi-v7a/libcrypto.so
        }
        equals(ANDROID_TARGET_ARCH, armeabi) {
            LIBS += -L$$PWD/libs/
            ANDROID_EXTRA_LIBS = $$PWD/libs/libfmodex.so $$PWD/libs-openssl/arch-armeabi/libssl.so $$PWD/libs-openssl/arch-armeabi/libcrypto.so 
        }
#        equals(ANDROID_TARGET_ARCH, x86)  {
#            [doAndroidx86Stuff]
#        }
    }

    server {
        # fmod
        no_fmod {
        DEFINES += "NO_FMOD=1"
        }
        DEFINES += "SERVER=1"
        linux {
            LIBS += -L$$PWD/libs2/linux/
        }
        windows {
            LIBS += -L$$PWD/libs2/windows/
        }
        # rtmidi
        linux {
            !use_jack {
                DEFINES += "__LINUX_ALSA__=1"
                LIBS += -lasound
            }
            use_jack {
                DEFINES += "__UNIX_JACK__=1"
                LIBS += -ljack -lasound
                TARGET = launchpadd-server-bin-jack
            }
        }
        windows {
            DEFINES += "__WINDOWS_MM__=1"
            LIBS += -lwinmm
        }
    }

DISTFILES += \
    android/AndroidManifest.xml \
    android/res/values/libs.xml \
    android/build.gradle \
    android/AndroidManifest.xml \
    android/res/values/libs.xml \
    android/build.gradle \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat
