import QtQuick 2.5
import QtQuick.Window 2.2

Window {
    visible: true
    width: Screen.width
    height: Screen.height
    visibility: "FullScreen"

    MainWindow {
        anchors.fill: parent
    }
}

