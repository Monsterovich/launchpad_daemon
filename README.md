![launchpaddlogo_small.png](https://bitbucket.org/repo/yGep9g/images/1802116299-launchpaddlogo_small.png)

Watch youtube demos: 

* https://www.youtube.com/watch?v=bGtdNfkY8fE
* https://www.youtube.com/watch?v=4gs-TxbNt3g

# Welcome to LaunchpadD repo! #

LaunchpadD is a fully configurable music creation tool for android platform. 

Current version: 1.5.1

**Features**

* Client/server support!
* MIDI support!
* Very light and easy GUI that's configured specially for your display size.
* Configurable amount of sound buttons.
* Large amount of button options & commands.
* You can touch multiple buttons at the same time.
* User-defined GUI style for buttons and borders.
* Supported multiple types of audio formats.
* Optimized to decrease sound latency: small audio buffers and all samples preloaded.

**System requirements**

* Android version: >= 4.4 starting with version 1.5.1, older versions support android 2.3.6

## Download ##

**Client:**

**Attention:** Client version 1.5.1 is fully compatible with 1.5 server.

* [Download](https://bitbucket.org/Monsterovich/launchpad_daemon/downloads/launchpadd-1.5.1.apk)

**Server:**

* [Download (windows)](https://bitbucket.org/Monsterovich/launchpad_daemon/downloads/launchpadd-server-1.5-win32.zip)
* [Download (linux 64-bit)](https://bitbucket.org/Monsterovich/launchpad_daemon/downloads/launchpadd-server-1.5-linux64.tar.bz2) (requires Qt 5 installed in your system: Qt5Core, Qt5Network)


**Donations:**

* PayPal: monsterovich@gmail.com
* Yandex money: 410014539584441
* BCH: bitcoincash:qq8dmyj6lycp4qwlpdquwghputkt4v5f9us6w3eqc4
