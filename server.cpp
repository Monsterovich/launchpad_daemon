#include "server.h"
#include "network.h"

UDPServer::UDPServer(quint16 port, QSoundButtons *sbuttons_pointer, QObject *parent) : QObject(parent)
{
	// set pointer to sbuttons
	sbuttons = sbuttons_pointer;

	// create a QUDP socket
	socket = new QUdpSocket(this);
	socket->bind(port);

	// call readyRead()
	connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()));

	Log("Listening on port " + QString::number(port));
}

void UDPServer::readInt16(quint16 &value, QIODevice &file)
{
	file.read((char*)&value, 2);
}

void UDPServer::readInt8(quint8 &value, QIODevice &file)
{
	file.read((char*)&value, 1);
}

void UDPServer::readyRead()
{
	while (socket->hasPendingDatagrams())
	{
		quint8 type = 0;
		quint16 data = 0;
		QByteArray buffer;
		buffer.resize(socket->pendingDatagramSize());

		QHostAddress sender;
		quint16 senderPort;

		socket->readDatagram(buffer.data(), buffer.size(), &sender, &senderPort);

		QBuffer packet(&buffer);
		packet.open(QIODevice::ReadOnly);

		readInt8(type, packet);
		readInt16(data, packet);

		switch(type)
		{
		case PT_PLAY:
			sbuttons->play(data);
			break;
		case PT_STOP:
			sbuttons->stop(data);
			break;
		case PT_HELLO:
			Log("Client from " + sender.toString() + ":" + QString::number(senderPort) + " said 'hello'!");
			break;
		case PT_BYE:
			Log("Client from " + sender.toString() + ":" + QString::number(senderPort) + " said 'bye'!");
			break;
		case PT_PRESET:
		{
			QString preset;
			for (int i = 0; i < data; i++)
			{
				quint8 c;
				readInt8(c, packet);
				preset.push_back(QChar(c));
			}
			Log("Recieved preset change request: " + preset);
			Log("Restarting...");
			sbuttons->applyPreset(preset);
			break;
		}
		default:
			break;
		}
	}
}
