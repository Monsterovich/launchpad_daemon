#ifndef COMMON_H
#define COMMON_H

#include <QDir>
#include <QString>
#include <QStringList>
#include <QVector>
#include <QMap>
#include <QSettings>
#include <QStandardPaths>
#include <QTextStream>
#include <QDebug>
#include <QDateTime>
#include <QProcess>
#include <QUdpSocket>
#include <QBuffer>
#include <QTimer>

#ifndef SERVER
#include <QObject>
#include <QByteArray>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#endif

#ifndef SERVER
#include <QScreen>
#endif

#endif // COMMON_H

