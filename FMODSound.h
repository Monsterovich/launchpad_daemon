#ifndef FMODSOUND_H
#define FMODSOUND_H

#include "sound.h"

#include "fmodex/fmod.h"
#include "fmodex/fmod_errors.h"

#define MAX_FMOD_CHANNELS 4093
#define FMOD_DEFAULT_FLAGS FMOD_CREATESAMPLE | FMOD_ACCURATETIME

typedef QMap <int, FMOD_CHANNEL*> QFMOD_CHANNELS;
typedef QMap <int, FMOD_SOUND*> QFMOD_SOUNDS;
typedef QMap <int, float> QFMOD_NOTERATIOS;

void FMOD_CheckVersion();

void FMOD_Init(FMOD_OUTPUTTYPE outtype, unsigned bufferlenght, int numbuffers);
void FMOD_PreCreateSound(int channel, QString filename, FMOD_MODE mode, const QString note);
void FMOD_SetBufferSize();
void FMOD_Play(int channel, bool backwards, FMOD_CHANNELINDEX index);
void FMOD_Stop(int channel);
void FMOD_DeInit(void);
void FMOD_SetVolume(int channel, float volume);
void FMOD_SetPanning(int channel, float pan);

unsigned int FMOD_GetSoundLength(int channel);
unsigned int FMOD_GetPosition(int channel);

#endif // FMODSOUND_H
