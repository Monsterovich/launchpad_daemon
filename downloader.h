#ifndef DOWNLOADER_H
#define DOWNLOADER_H

#include "misc.h"

class QFileDownloader : public QObject
{
	Q_OBJECT
public:
	explicit QFileDownloader(QUrl Url, QObject *parent = nullptr);
	virtual ~QFileDownloader() {}
	const QByteArray downloadedData();
	double getStatusValue();

signals:
	void downloaded();
	void statusUpdate();

private slots:
	void fileDownloaded(QNetworkReply* pReply);
	void progressChanged(qint64 read, qint64 total);

private:
	QNetworkAccessManager m_WebCtrl;
	QByteArray m_DownloadedData;
	qint64 m_Read, m_Total;
};

#define DEFAULT_PRESET_URL "https://bitbucket.org/Monsterovich/launchpad_daemon/raw/c4238fdbdb9ea32e3d19a98a8d81d919e38fffe7/launchpad_resources.tar"

#endif // DOWNLOADER_H
