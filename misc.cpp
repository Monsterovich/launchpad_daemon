#include "misc.h"

#ifndef SERVER
QFile *logfile;
#endif

extern QSettings *set;
extern QString homedir;

void ProcessForKeyWords(QString &string)
{
	QString newstring = string;
	QString keyword;
	bool started = false;
	for (int i = 0; i < string.length(); i++)
	{
		QChar symbol = string[i];
		if (symbol == '%')
		{
			if (!started)
			{
				started = true;
				keyword = "";
				continue;
			}

			if (started)
			{
				started = false;

				set->beginGroup("Keyword");
				QString val = set->value(keyword).toString();
				set->endGroup();

				if (!val.isEmpty())
					newstring.replace('%' + keyword + '%', val);

				continue;
			}
		}
		if (started)
			keyword += symbol;
	}
	string = newstring;
}

void CreateNoMediaFile()
{
#ifndef SERVER
	QFile checker(HomeDirectory() + NOMEDIA);

	if (checker.exists())
		return;

	checker.open(QIODevice::WriteOnly);
	checker.close();
#endif
}

void CreateHomeDirectory()
{
#ifndef SERVER
	QDir dir(PROGDIR);
	if(!dir.exists())
		dir.mkdir(PROGDIR);
#endif
}

void CreateLogFile()
{
#ifndef SERVER
	if (!logfile)
		logfile = new QFile(HomeDirectory() + LOGFILE);

	logfile->open(QIODevice::WriteOnly);
#endif
}

const QString HomeDirectory()
{
#ifndef SERVER
	return PROGDIR;
#else
	return homedir;
#endif
}

void Log(const QString msg, bool nextline)
{
	QDateTime time;
	time = time.currentDateTime();
	QString dstr = time.toString("[hh:mm:ss] ") + msg;

#ifndef SERVER
	qDebug() << dstr;
	(void)nextline;
#else
	if (nextline)
		printf("%s\n", dstr.toStdString().c_str());
	else
		printf("%s", dstr.toStdString().c_str());
#endif
#ifndef SERVER
	QTextStream ts(logfile);
	ts << dstr << endl;
#endif
}

void Print(const QString msg)
{
	printf("%s", msg.toStdString().c_str());
}


void CloseLog()
{
#ifndef SERVER
	Log("Closing log file.");
	logfile->close();
#endif
}

void Crash()
{
	Log("Launchpadd has been terminated...");
	exit(1);
}
