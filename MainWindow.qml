import QtQuick 2.6
import QtQuick.Controls 2.2
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Window 2.0
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import QtQml 2.3

import QtQuick.Controls 1.4 as Old

Rectangle {
    property string helpurl: "https://bitbucket.org/Monsterovich/launchpad_daemon/wiki/Home"
    property int z_top: 99999;

    Menu {
        id: settingsMenu
        width: 300
        title: "Client settings"
        x: Screen.width / 2 - width / 2
        y: Screen.height / 2 - height / 2
        modal: true
        onClosed: {
            parent.focus = true;
            currentIndex = -1
        }

        CheckBox {
            id: connect
            checked: server_connect
            leftPadding: 10
            text: "Client/server mode"
        }
        Text {
            padding: 10
            font.bold: true
            width: parent.width
            horizontalAlignment: Qt.AlignHCenter
            text: "Address"
        }
        TextField {
            id: address
            selectByMouse: true
            font.pixelSize: 20
            leftPadding: 10
            verticalAlignment: TextInput.AlignVCenter
            width: parent.width
            text: server_address
            enabled: connect.checked
        }
        Text {
            padding: 10
            font.bold: true
            width: parent.width
            horizontalAlignment: Qt.AlignHCenter
            text: "Port"
        }
        TextField {
            id: port
            selectByMouse: true
            font.pixelSize: 20
            leftPadding: 10
            inputMethodHints: Qt.ImhDigitsOnly
            verticalAlignment: TextInput.AlignVCenter
            width: parent.width
            text: server_port
            enabled: connect.checked
        }
        RowLayout {
            MenuItem {
                Layout.fillWidth: true
                Text {
                    anchors.centerIn: parent
                    text: "Cancel"
                }
                onTriggered: {
                    settingsMenu.parent.focus = true
                    settingsMenu.close()
                }
            }
            MenuItem {
                Layout.fillWidth: true
                Text {
                    anchors.centerIn: parent
                    text: "Apply"
                }
                onTriggered: {
                    settingsMenu.parent.focus = true
                    settingsMenu.close()
                    sbuttons.applySettings(address.text, port.text, connect.checked)
                }
            }
        }
    }

    Menu {
        id: mainMenu
        title: "Main menu"
        onClosed: {
            parent.focus = true;
            currentIndex = -1
        }

        property real margin: 6
        width: Screen.width - (margin*2)
        x: margin
        y: Screen.height - height - margin

        Menu {
            id: presetMenu
            title: "Change preset"
            onClosed: mainMenu.open()

            property real margin: mainMenu.margin
            width: Screen.width - (margin*2)
            height: 200 + margin
            x: margin
            y: Screen.height - height - margin

            ListView {
                id: presetList
                anchors.centerIn: parent
                height: presetMenu.height

                ScrollBar.vertical: ScrollBar { 
                    active: true
                    width: 5

                    contentItem: Rectangle {
                        radius: implicitHeight / 2
                        color: "grey"
                    }
                }
                clip: true

                ListModel {
                    id: presetModel
                    ListElement {
                        presets: []
                    }
                }
                model: presetModel
                delegate: Column {
                    Repeater {
                        model: presets
                        MenuItem {
                            id: presetItem
                            text: presetName
                            font.bold: index === 0
                            width: presetList.width
                            onTriggered: { 
                                presetMenu.close()
                                downloadBackground.visible = true
                                pleaseWait.visible = true
                                restartTimer.preset = text
                                restartTimer.start()
                            }
                        }
                    }
                    Rectangle {
                        width: presetList.width
                        height: mainMenu.margin * 2
                    }
                }
                Component.onCompleted: {
                    var list = sbuttons.generatePresetList();
                    for (var i = 0; i < list.length; i++)
                        presetModel.get(0).presets.append({ presetName : list[i] })
                }
            }
        }

        MenuItem {
            text: "Client settings"
            onTriggered: settingsMenu.open()
        }

        MenuItem {
            text: "Help"
            onTriggered: Qt.openUrlExternally(helpurl)
        }

        MenuItem {
            text: "Quit"
            onTriggered: Qt.quit()
        }
    }
    Timer {
        id: restartTimer
        interval: 1
        running: false
        repeat: false
        property variant preset: ""
        onTriggered: sbuttons.applyPreset(preset)
    }
    Rectangle {
        id: downloadBackground
        z: z_top - 1
        width: Screen.width; height: Screen.height
        color: "#80000000"
        visible: false
        MouseArea {
            anchors.fill: parent
            enabled: parent.visible
        }
    }
    Text {
        id: pleaseWait
        visible: false
        anchors.centerIn: parent
        text: "Please wait..."
        color: "white"
        z: z_top
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }
    Old.ProgressBar
    {
        id: downloadBar
        z: z_top
        x: Screen.width / 2 - 40
        y: Screen.height / 2 - 40
        value: downloadProgress
        visible: false
        style: ProgressBarStyle
        {
           panel : Rectangle
           {
              color: "transparent"
              implicitWidth: 80
              implicitHeight: implicitWidth
        
              Rectangle
              {
                 id: outerRing
                 z: 0
                 anchors.fill: parent
                 radius: Math.max(width, height) / 2
                 color: "transparent"
                 border.color: "red"
              }
        
              Rectangle
              {
                 id: innerRing
                 z: 1
                 anchors.fill: parent
                 anchors.margins: (outerRing.border.width - border.width) / 2
                 radius: outerRing.radius
                 color: "transparent"
                 border.color: "white"
                 border.width: 4
        
                 ConicalGradient
                 {
                    source: innerRing
                    anchors.fill: parent
                    gradient: Gradient
                    {
                       GradientStop { position: 0.00; color: "red" }
                       GradientStop { position: control.value; color: "red" }
                       GradientStop { position: control.value + 0.01; color: "transparent" }
                       GradientStop { position: 1.00; color: "transparent" }
                    }
                 }
              }
        
              Text
              {
                 id: progressLabel
                 anchors.centerIn: parent
                 color: "black"
                 text: (control.value * 100).toFixed() + "%"
              }
           }
        }
    }

    MessageDialog 
    {
        id: errorDialog
        standardButtons: StandardButton.Yes | StandardButton.No
        title: "Welcome to Launchpadd!"
        text: "Seems it's your first launch. We cannot find 'launchpadd.ini' configuration file in '/sdcard/.launchpadd/' Download default preset automatically?"
        onNo: {
            Qt.quit()
        }
        onYes: {
            downloadBackground.visible = true
            downloadBar.visible = true
            sbuttons.downloadDefaultPreset()
            visible = false
        }
        visible: !iniExists
    }

    MessageDialog 
    {
        id: presetInstallSuccessDialog
        title: "Done"
        text: "Default preset was successfully installed! Press OK to restart."
        onAccepted: {
            sbuttons.restartApplication()
        }
        visible: presetInstallSuccess
    }

    MessageDialog 
    {
        id: launchDialog
        title: "Hello there!"
        standardButtons: StandardButton.Ok
        text: "Welcome to launchpadd. Press back button to open main menu. Good luck! :)"
        visible: iniExists && !iniNull && !presetiniNull && presetiniExists && !restart
        onAccepted: {
            // nothing
        }
    }

    MessageDialog 
    {
        id: presetInstallErrorDialog
        title: "It can't be!"
        text: "There is a trouble when trying to unpack or download the archive. Check your internet connection. Also check /sdcard/.launchpadd/ "
        onAccepted: {
            Qt.quit()
        }
        visible: presetInstallError
    }

    MessageDialog 
    {
        id: errorDialog2
        title: "Fatal error!!!"
        text: "Failed to load 'launchpadd.ini' configuration file. This should not happen!"
        onAccepted: {
            Qt.quit()
        }
        visible: iniNull
    }

    MessageDialog 
    {
        id: errorDialog3
        title: "Error!"
        text: "Cannot find 'preset.ini' configuration file. Check 'launchpadd.ini'."
        onAccepted: {
            Qt.quit()
        }
        visible: !presetiniExists && iniExists
    }

    MessageDialog 
    {
        id: errorDialog4
        title: "Fatal error!"
        text: "Failed to load 'preset.ini' configuration file. This should not happen!"
        onAccepted: {
            Qt.quit()
        }
        visible: presetiniNull && !iniNull
    }

    // rect properties
    id: rectBody
    color: backgroundColor
    anchors.fill: parent
    focus: true

    Keys.onReleased: {
           if(event.key === Qt.Key_Back) 
           {
               event.accepted = true;
               mainMenu.open()
           }
       }

    MultiPointTouchArea {
        id: touchArea
        anchors.fill: parent
        maximumTouchPoints: 10
        minimumTouchPoints: 1
        objectName: "buttons"
        touchPoints: [
            TouchPoint { property int tid: 0 },
            TouchPoint { property int tid: 1 },
            TouchPoint { property int tid: 2 },
            TouchPoint { property int tid: 3 },
            TouchPoint { property int tid: 4 },
            TouchPoint { property int tid: 5 },
            TouchPoint { property int tid: 6 },
            TouchPoint { property int tid: 7 },
            TouchPoint { property int tid: 8 },
            TouchPoint { property int tid: 9 }
        ]

        property variant lastPressedItem: []
        Component.onCompleted: {
            for (var i = 0; i < maximumTouchPoints; i++) {
                lastPressedItem.push(-1)
            }
        }

        // Workaround for item.contains(point)
        function isInsideItem(item, point) {
            var hx = item.x + item.width;
            var lx = item.x;
            var hy = item.y + item.height;
            var ly = item.y;

            if (point.x > lx && point.x < hx && point.y > ly && point.y < hy)
                return true;

            return false;
        }

        function setRunningAnim(id, running)
        {
            var item = repeater.itemAt(id-1)
            item.runningAnim = running;
            item.color = sbuttons.getButtonColor(id)
        }

        onPressed: {
           for (var i = 0; i < repeater.count; i++) {
               var item = repeater.itemAt(i)
               for (var j = 0; j < touchPoints.length; j++) {
                   var point = touchPoints[j];
                   if (isInsideItem(item, point)) {
                       sbuttons.play(i+1)
                       lastPressedItem[point.tid] = i
                       if (sbuttons.getButtonColorPolicy(i+1)) {
                           if(item.runningAnim) {
                               item.runningAnim = false;
                               item.color = sbuttons.getButtonColor(i+1)
                           }
                           else
                               item.runningAnim = true;
                       }
                       else
                           item.color = sbuttons.getPressedButtonColor(i+1)
                   }
               }
           }
        }

        onReleased: {
            for (var i = 0; i < repeater.count; i++) {
                var item = repeater.itemAt(i)
                for (var j = 0; j < touchPoints.length; j++) {
                    var point = touchPoints[j];
                    if (isInsideItem(item, point)) {
                        sbuttons.stop(i+1)
                        lastPressedItem[point.tid] = -1

                        if (!sbuttons.getButtonColorPolicy(i+1))
                            item.color = sbuttons.getButtonColor(i+1)
                    }
                    else {
                        if (lastPressedItem[point.tid] >= 0) {
                            var newItem = repeater.itemAt(lastPressedItem[point.tid])
                            newItem.color = sbuttons.getButtonColor(lastPressedItem[point.tid]+1)
                            lastPressedItem[point.tid] = -1
                        }
                    }
                }
            }
        }

        GridLayout {
            anchors.fill: parent
            id: grid
            rowSpacing: 0
            columnSpacing: 0
            columns: padColumns
            rows: padRows

            property double colMulti : grid.width / grid.columns;
            property double rowMulti : grid.height / grid.rows;
            function prefWidth(item){
                return colMulti * item.Layout.columnSpan;
            }
            function prefHeight(item){
                return rowMulti * item.Layout.rowSpan;
            }

            Repeater {
                id: repeater
                model: padColumns*padRows
                Rectangle {
                    property bool runningAnim: false
                    Layout.preferredWidth: grid.prefWidth(this)
                    Layout.preferredHeight: grid.prefHeight(this)
                    width: 48
                    height: 48
                    color: sbuttons.getButtonColor(index+1)
                    SequentialAnimation on color {
                        id: colorAnim
                        loops: Animation.Infinite
                        running: runningAnim

                        ColorAnimation {
                            to: sbuttons.getPressedButtonColor(index+1)
                            duration: sbuttons.getButtonSoundLength(index+1) / 2

                        }
                        ColorAnimation {
                            to: sbuttons.getButtonColor(index+1)
                            duration: sbuttons.getButtonSoundLength(index+1) / 2
                        }
                    }
                    Text {
                        visible: runningAnim && !clientsideonly
                        anchors.centerIn: parent
                        width: 0
                        height: 0
                        text: ""
                        color: "black"
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        rotation: 90
                        Timer {
                            id: updateTimer
                            interval: 1
                            running: runningAnim && !clientsideonly
                            repeat: true
                            onTriggered: parent.text = (sbuttons.getButtonChannelPosition(index+1) / sbuttons.getButtonSoundLength(index+1) * 100).toFixed() + "%"
                        }
                    }

                    border.color: sbuttons.getButtonBorderColor(index+1)
                    border.width: 1
                    radius: 5
                }
            }
        }
    }
}
