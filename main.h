#ifndef MAIN_H
#define MAIN_H

#include "misc.h"
#include "FMODSound.h"
#ifndef SERVER
#include "client.h"
#include "downloader.h"
#else
#include "MIDISound.h"
#endif

#ifndef SERVER
#include <QQmlApplicationEngine>
#include <QGuiApplication>
#else
#include <QCoreApplication>
#endif

class QSoundButtons : public QObject
{
	Q_OBJECT
public:
#ifndef SERVER
	QSoundButtons(QQmlApplicationEngine *engine_pointer, QGuiApplication *app_pointer);
#else
	QSoundButtons(QCoreApplication *app_pointer);
#endif
	void SetSize(int r, int c) { rows = r; columns = c; }
	void PreCreateSounds();
#ifdef SERVER
	void restartApplication();
	void applyPreset(QString preset);
#endif

#ifndef SERVER
	UDPClient client;
#endif
signals:

public slots:
	Q_INVOKABLE void play(const int button);
	Q_INVOKABLE void stop(const int button);
#ifndef SERVER
	Q_INVOKABLE void downloadDefaultPreset();
	Q_INVOKABLE void restartApplication();
	Q_INVOKABLE QVariant generatePresetList();
	Q_INVOKABLE void applyPreset(QString preset);
	Q_INVOKABLE void applySettings(QString address, QString port, bool connect);
#endif

#ifndef SERVER
	Q_INVOKABLE bool getButtonColorPolicy(const int button);

	Q_INVOKABLE const QString getPressedButtonColor(const int button);
	Q_INVOKABLE const QString getButtonColor(const int button);
	Q_INVOKABLE const QString getButtonBorderColor(const int button);

	Q_INVOKABLE unsigned int getButtonSoundLength(const int button);
	Q_INVOKABLE unsigned int getButtonChannelPosition(const int button);

	void preset_downloaded();
	void download_status_changed();
#endif

private:
	const QString GetPositionString(const int button);
	int GetPositionNumber(const QString button);

#ifndef SERVER
	void SetButtonAnimation(int id, bool active);
#endif
private:
	int rows, columns;
	QMap<int, bool> used;
private:
#ifndef SERVER
	QQmlApplicationEngine *engine;
	QGuiApplication *app;
	QFileDownloader *download_manager;
#else
	QCoreApplication *app;
#endif
};

#endif // MAIN_H

