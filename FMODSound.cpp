#include "FMODSound.h"

// basic
FMOD_SYSTEM  *gSystem  = NULL;
QFMOD_CHANNELS gChannels;
QFMOD_SOUNDS gSounds;
QFMOD_NOTERATIOS nratios;

// debug
FMOD_RESULT result;
void FMOD_ErrCheck(FMOD_RESULT result, int line)
{
	if (result != FMOD_OK)
		Log("FMOD error (" + QString::number(result) + ") at line " + QString::number(line) + ": " + QString(FMOD_ErrorString(result)));
}
#define ERRCHECK(result) FMOD_ErrCheck(result, __LINE__)

void FMOD_CheckVersion()
{
	unsigned version;
	result = FMOD_System_GetVersion(gSystem, &version);
	ERRCHECK(result);

	char libhex[16], hex[16];
	sprintf(libhex, "%#010x", version);
	sprintf(hex, "%#010x", FMOD_VERSION);

	if (version == FMOD_VERSION)
		Log("Library version: " + QString(libhex) + " =  FMOD version: " + QString(hex) + " - OK!");
	else
	{
		Log("Library version: " + QString(libhex) + " !=  FMOD version: " + QString(hex));
		Log("Warning: this may cause some problems!");
	}
}

//
// FMOD STUFF
//

void FMOD_Init(FMOD_OUTPUTTYPE outtype, unsigned bufferlenght, int numbuffers)
{
	result = FMOD_System_Create(&gSystem);
	ERRCHECK(result);
	result = FMOD_System_SetOutput(gSystem, outtype);
	ERRCHECK(result);
	result = FMOD_System_SetDSPBufferSize(gSystem, bufferlenght, numbuffers);
	ERRCHECK(result);
	result = FMOD_System_Init(gSystem, MAX_FMOD_CHANNELS, FMOD_INIT_NORMAL, 0);
	ERRCHECK(result);
}

void FMOD_DeInit(void)
{
	result = FMOD_System_Release(gSystem);
	ERRCHECK(result);
	// throws an error
	//result = FMOD_System_Close(gSystem);
	//ERRCHECK(result);
}

void FMOD_PreCreateSound(int channel, QString filename, FMOD_MODE mode, const QString note)
{
	result = FMOD_System_CreateSound(gSystem, filename.toStdString().c_str(), FMOD_DEFAULT_FLAGS | mode, 0, &gSounds[channel]);
	ERRCHECK(result);
	GetFrequencyRatio(nratios[channel], note);
}

void FMOD_Play(int channel, bool backwards, FMOD_CHANNELINDEX index)
{
	float freq;

	result = FMOD_System_PlaySound(gSystem, index, gSounds[channel], true, &gChannels[channel]);
	ERRCHECK(result);
	result = FMOD_Channel_GetFrequency(gChannels[channel], &freq);
	ERRCHECK(result);

	// frequency control
	freq *= nratios[channel];
	if (backwards)
		freq *= -1;

	result = FMOD_Channel_SetFrequency(gChannels[channel], freq);
	ERRCHECK(result);
	result = FMOD_Channel_SetPaused(gChannels[channel], false);
	ERRCHECK(result);

	// update system & channels
	result = FMOD_System_Update(gSystem);
	ERRCHECK(result);
}

void FMOD_Stop(int channel)
{
	result = FMOD_Channel_Stop(gChannels[channel]);
	ERRCHECK(result);
}

void FMOD_SetVolume(int channel, float volume)
{
	result = FMOD_Channel_SetVolume(gChannels[channel], volume);
	ERRCHECK(result);
}

void FMOD_SetPanning(int channel, float pan)
{
	result = FMOD_Channel_SetPan(gChannels[channel], pan);
	ERRCHECK(result);
}

unsigned int FMOD_GetSoundLength(int channel)
{
	if (!gSounds[channel])
		return 0;
	unsigned int length;
	result = FMOD_Sound_GetLength(gSounds[channel], &length, FMOD_TIMEUNIT_MS);
	ERRCHECK(result);
	return length;
}

unsigned int FMOD_GetPosition(int channel)
{
	if (!gSounds[channel])
		return 0;
	unsigned int position;
	result = FMOD_Channel_GetPosition(gChannels[channel], &position, FMOD_TIMEUNIT_MS);
	ERRCHECK(result);
	return position;
}


