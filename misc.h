#ifndef MISC_H
#define MISC_H

#include "common.h"

#define LOGFILE "launchpadd.log"
#define INIFILE "launchpadd.ini"
#define PRESETINI "preset.ini"

#ifndef SERVER
#define PROGDIR QString("/mnt/sdcard/.launchpadd/")
#define NOMEDIA QString(".nomedia")
#endif

#define BoolQStr(a) QString(a ? "true" : "false")

#define SEPARATOR "------------------------"

void CreateNoMediaFile();
void CreateHomeDirectory();
void CreateLogFile();

void ProcessForKeyWords(QString &string);
const QString HomeDirectory();
void Log(const QString msg, bool nextline = true);
void Print(const QString msg);
void CloseLog();
void Crash();

#endif // MISC_H
