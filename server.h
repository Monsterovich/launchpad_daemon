#ifndef SERVER_H
#define SERVER_H

#include "misc.h"
#include "main.h"

class UDPServer : public QObject
{
	Q_OBJECT
public:
	explicit UDPServer(quint16 port, QSoundButtons *sbuttons_pointer, QObject *parent = 0);

public slots:
	void readyRead();

private:
	void readInt16(quint16 &value, QIODevice &file);
	void readInt8(quint8 &value, QIODevice &file);

private:
	QSoundButtons *sbuttons;
	QUdpSocket *socket;
};

#endif // SERVER_H
