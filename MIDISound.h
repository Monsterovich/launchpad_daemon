#ifndef MIDISOUND_H
#define MIDISOUND_H

#include "misc.h"

#include "rtmidi/RtMidi.h"

typedef QVector<uchar> QRTMIDI_MSG;

void RTMIDI_Init();
void RTMIDI_DeInit();
void RTMIDI_SendMessage(QRTMIDI_MSG message);
void RTMIDI_OpenPort(unsigned port);
void RTMIDI_PrintPortInfo();
const QString RTMIDI_GetApiName();

#endif // MIDISOUND_H
