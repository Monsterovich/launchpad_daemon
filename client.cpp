#include "client.h"

UDPClient::UDPClient(QObject *parent) : QObject(parent)
{
	// create a QUDP socket
	socket = new QUdpSocket(this);
	active = false;
}

void UDPClient::Connect(QString address_str, quint16 newport)
{
	address = QHostAddress(address_str);
	port = newport;
	Log("Connecting to " + address.toString() + ":" + QString::number(port));

	// send 'hello' to the server
	SendPacket(PT_HELLO);
}

void UDPClient::appendInt16(QByteArray &array, quint16 value)
{
	array.append((char*)&value, 2);
}

void UDPClient::appendInt8(QByteArray &array, quint8 value)
{
	array.append((char*)&value, 1);
}

void UDPClient::SendPacket(PacketType type, quint16 data)
{
	QByteArray buffer;

	appendInt8(buffer, type);
	appendInt16(buffer, data);

	socket->writeDatagram(buffer, address, port);
}

void UDPClient::SendPacketString(PacketType type, QString str)
{
	QByteArray buffer;

	appendInt8(buffer, type);
	appendInt16(buffer, (quint16)str.length());
	for (int i = 0; i < str.length(); i++)
		appendInt8(buffer, (quint8)str[i].toLatin1());

	socket->writeDatagram(buffer, address, port);
}
